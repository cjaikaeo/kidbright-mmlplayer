import buzzer
from mmlparser import MMLParser, NOTES


class MMLPlayer:
    """MML (Music Macro Language) Player for KidBright boards"""

    def __init__(self):
        self._parser = MMLParser(num_channels=1, callback=self._callback)

    def _callback(self, channel, evt, note, vel):
        if evt == "on":
            try:
                buzzer.on(NOTES[note])
            except IndexError:
                pass
        elif evt == "off":
            buzzer.off()
        
    def play(self, notes):
        """Play musical notes written in MML."""
        self._parser.play(notes)

    async def aplay(self, notes):
        """Asynchronously play musical notes written in MML."""
        await self._parser.aplay(notes)

