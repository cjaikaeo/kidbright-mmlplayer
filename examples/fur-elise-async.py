from machine import Pin
import uasyncio as asyncio
from mmlplayer import MMLPlayer

async def task_music():
    player = MMLPlayer()
    
    # MML code obtained from https://archeagemmllibrary.com/beethoven-fur-elise-2/
    # with the second channel removed
    await player.aplay("""
        v127t65l16>ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-dc
        <a8rceab8rdb+ba8rb>cde8.<g>fed8.<f>edc8.<e>dc<br8r>er8
        r>er8<d+er8d+ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-d
        c<a8rceab8rdb+ba8
    """)

async def task_blink():
    led = Pin(2, Pin.OUT)
    while True:
        led.value(1-led.value())
        await asyncio.sleep(0.5)
        
loop = asyncio.get_event_loop()
loop.create_task(task_music())
loop.create_task(task_blink())
loop.run_forever()

