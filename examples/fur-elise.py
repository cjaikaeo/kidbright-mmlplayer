from mmlplayer import MMLPlayer

player = MMLPlayer()

# MML code obtained from https://archeagemmllibrary.com/beethoven-fur-elise-2/
# with the second channel removed
player.play("""
    v127t65l16>ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-dc
    <a8rceab8rdb+ba8rb>cde8.<g>fed8.<f>edc8.<e>dc<br8r>er8
    r>er8<d+er8d+ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-d
    c<a8rceab8rdb+ba8
""")

