# MML Player for KidBright

This project provides functionality to play music written in MML (Music Macro
Library) on KidBright.

## Dependencies
The code requires the following libraries to be installed on KidBright.
* The [buzzer.py](https://github.com/microBlock-IDE/micropython/blob/master/ports/esp32/boards/KidBright32/modules/buzzer.py) module developed by [Sonthaya Nongnuch](https://github.com/maxpromer)
* The [mmlparser.py](https://gitlab.com/cjaikaeo/mmlparser-python/-/blob/master/mmlparser/mmlparser.py) module developed by [Chaiporn Jaikaeo](https://gitlab.com/cjaikaeo)

## Examples

### Playing MML Notes
All the delays are performed by calling `time.sleep`.  The execution will block until the entire music is done playing.

```python
from mmlplayer import MMLPlayer

player = MMLPlayer()

# MML code obtained from https://archeagemmllibrary.com/beethoven-fur-elise-2/
# with the second channel removed
player.play("""
    v127t65l16>ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-dc
    <a8rceab8rdb+ba8rb>cde8.<g>fed8.<f>edc8.<e>dc<br8r>er8
    r>er8<d+er8d+ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-d
    c<a8rceab8rdb+ba8
""")
```

### Asynchronously Playing MML Notes
The library provides the method `aplay` which is an async version of the method `play`, based on the `uasyncio` module.
The code below concurrently plays a music and blinks the red LED.

```python
from machine import Pin
import uasyncio as asyncio
from mmlplayer import MMLPlayer

async def task_music():
    player = MMLPlayer()
    
    # MML code obtained from https://archeagemmllibrary.com/beethoven-fur-elise-2/
    # with the second channel removed
    await player.aplay("""
        v127t65l16>ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-dc
        <a8rceab8rdb+ba8rb>cde8.<g>fed8.<f>edc8.<e>dc<br8r>er8
        r>er8<d+er8d+ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-d
        c<a8rceab8rdb+ba8
    """)

async def task_blink():
    led = Pin(2, Pin.OUT)
    while True:
        led.value(1-led.value())
        await asyncio.sleep(0.5)
        
loop = asyncio.get_event_loop()
loop.create_task(task_music())
loop.create_task(task_blink())
loop.run_forever()
```
